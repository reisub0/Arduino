#include <StaticThreadController.h>
#include <Thread.h>
#include <ThreadController.h>

#define GSM Serial3

char *SERVERCONFIG = "AT+CIPSTART=\"TCP\",\"reisub.ddns.net\",\"1234\"\r";

bool unreadSMSFlag = false;

void (*resetFunc)(void) = 0;

char GSMBuffer[1000];

// ThreadController that will controll all threads
ThreadController controll = ThreadController();

// Thread to handle incoming SMSes
Thread *SMSThread = new Thread();

void UnreadSMSThread() {
  if (unreadSMSFlag) {
    Serial.println("Unread SMS Detected! Running SMS Code");
    getGSMResponse("AT+CMGR=1");
    unreadSMSFlag = false;
  }
}

void printTime() {}

int day, month, year, minute, second, hour;

void setup() {
  Serial.begin(9600);
  GSM.begin(9600);
  initGSM();
  initGPRS();
  connectServer();
  SMSThread->onRun(UnreadSMSThread);
  SMSThread->setInterval(1000);
  controll.add(SMSThread);
}

void loop() {
  controll.run();
  while (Serial.available()) {
    Serial3.write(Serial.read());
  }
  readFromGSM();
  /* Serial.print("Flag = "); */
  /* Serial.println(unreadSMSFlag); */
}

void initGSM() {
  /* Reset the module first */
  tryUntilResponse("AT+CFUN=1,1", "OK");
  /* Give it some time to start up */
  delay(10000);
  tryUntilResponse("ATE0", "OK");
  tryUntilResponse("AT+CNMI=1,1,0,0,0", "OK");
  tryUntilResponse("AT+CLTS=1", "OK");
  /* tryUntilResponse("AT+COPS=2", "OK"); */
  /* tryUntilResponse("AT+COPS=0", "OK"); */
  tryUntilResponse("AT+CMGF=1", "OK");
  tryUntilResponse("AT&W", "OK");

  tryUntilResponse("AT+CPIN?", "READY");
  /* Ensure text mode is set for SMSes */
  tryUntilResponse("AT+CMGF=1", "OK");
  /* Delete all messages */
  tryUntilResponse("AT+CMGD=1,4", "OK");
  /* tryUntilResponse("AT", "OK"); */
  /* tryUntilResponse("ATE0", "OK"); */
  /* tryUntilResponse("AT+CPIN?", "READY"); */
  /* tryUntilResponse("AT+CLTS=1", "OK"); */
  /* tryUntilResponse("AT+COPS=2", "OK"); */
  /* tryUntilResponse("AT+COPS=0", "OK"); */
  /* tryUntilResponse("AT&W", "OK"); */
  Serial.println("Done initing GSM");
}

void initGPRS() {
  tryUntilResponse("AT+CIPSHUT", "OK");
  tryUntilResponse("AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\"\r", "OK");
  tryUntilResponse("AT+SAPBR=3,1,\"APN\",\"airtelgprs.com\"\r", "OK");
  putCMD("AT+SAPBR=1,1");
  delay(1000);

  /* putCMD("AT+CFUN=1,1"); */
  tryUntilResponse("AT+CIPMUX=0", "OK");
  delay(5000);
  Serial.println("Done initing GPRS");
}

void connectServer() {
  String resp = "";
  while (true) {
    putCMD(SERVERCONFIG);
    resp = readFromGSM();
    if (resp.indexOf("FAIL") > 0) {
      delay(2000);
      continue;
    } else if (resp.indexOf("ALREADY") > 0) {
      return;
    }
  }
}

void putCMD(char *cmd) {
  GSM.print(cmd);
  GSM.write('\r');
  GSM.write('\n');
}

String readFromGSM() {
  String GSMBuffer = "";
  while (GSM.available() == 0)
    ;
  while (GSM.available() > 0) {
    GSMBuffer += (char)GSM.read();
    delay(10);
  }
  if (GSMBuffer.indexOf("+CMTI") >= 0) {
    unreadSMSFlag = true;
    GSMBuffer = "";
    return GSMBuffer;
  }
  if (GSMBuffer != "") {
    Serial.print("Got: \"");
    /* Serial.print(GSMBuffer); */
    for (int i = 0; i < GSMBuffer.length(); i++) {
      Serial.write(GSMBuffer.charAt(i));
    }
    Serial.println("\"");
  }
  return GSMBuffer;
}

/* Helper function that returns the string that the GSM Module responded with */
String getGSMResponse(char *cmd) {
  String response = "";
  putCMD(cmd);
  response = readFromGSM();
  return response;
}

/* Keep trying a command until the response matches with waitTime of x ms */
void tryUntilResponse(char *cmd, String res) {
  while (1) {
    Serial.print("Sent command:");
    Serial.println(cmd);
    if (getGSMResponse(cmd).indexOf(res) > 0) {
      return;
    }
    /* delay(waitTime); */
    delay(2000);
    /* Serial.flush(); */
    /* GSM.flush(); */
  }
}
