#include <AltSoftSerial.h>
#include <EEPROM.h>

#undef DAYS_PER_WEEK
#define gpsPort Serial1
#define GPS_PORT_NAME "Serial1"
#define DEBUG_PORT Serial

#include <NMEAGPS.h>

#include <SoftwareSerial.h>

// SoftwareSerial RFID1(10, 11);
#define RFID1 Serial2
// SoftwareSerial RFID2(46, 48);
// #define RFID2 Serial1
#define baudrate 9600
#define BUZZER 8
#define EEPROMAddr 0
#define GSM Serial3

#define DEBUG

#ifdef DEBUG
#define D(x) x
#else
#define D(x)
#endif

AltSoftSerial RFID2; // Uses 46,48 as Pins

static NMEAGPS gps; // This parses the GPS characters
static gps_fix fix; // This holds on to the latest values

struct EEPROMStruct {
  bool GSMResetFlag;
  char IMEI[20];
  char APN[20];
  char IP[20];
  char port[10];
  int delaytime;
};

// Since there is no SMS detect thread preconfigure

// EEPROMStruct Settings;
EEPROMStruct Settings = {false,           "0",     "airtelgprs.net",
                         "139.59.70.218", "10331", 2};

/* void setup() { */
/*   Serial.begin(baudrate); */
/*   D(Serial.println("# Out setup()")); */
/*   Serial.println("Started up"); */
/* } */
/* void setup() { */
/*   Serial.begin(baudrate); */
/*   D(Serial.println("# In setup()")); */

/*   // EEPROM.put(EEPROMAddr, Settings); */
/*   // EEPROM.get(EEPROMAddr, Settings); */

/*   GSM.begin(baudrate); */
/*   gpsPort.begin(baudrate); */
/*   RFID1.begin(baudrate); */
/*   RFID2.begin(baudrate); */

/*   pinMode(BUZZER, OUTPUT); */

/*   updateLocation(); */

/*   // setTimeFromGSM(); */
/*   // EEPROM.put(EEPROMAddr, Settings); */
/*   endData(); */
/*   initGSM(); */
/*   initGPRS(); */
/*   updateIMEI(); */
/*   D(Serial.println("# in connectServer()")); */
/*   connectServer(); */
/*   D(Serial.println("# Out setup()")); */
/*   Serial.println("Started up"); */
/* } */

char RFIDBuff[200] = {0};

// TEST LOOP
/* void loop() { */
/*   // while (RFID1.available()) { */
/*   //   D(Serial.print(RFID1.read())); */
/*   // } */
/*   // D(Serial.println()); */
/*   D(Serial.print("Dryscale : ")); */
/*   D(Serial.println(getWeight(1))); */
/*   D(Serial.print("Wetscale : ")); */
/*   D(Serial.println(getWeight(2))); */
/*   // if (RFID1.available()) { */
/*   /1* D(Serial.print("RFID1: ")); *1/ */
/*   /1* RFID1.readBytes(RFIDBuff, 12); *1/ */
/*   /1* D(Serial.println(RFIDBuff)); *1/ */
/*   /1* connectServer(); *1/ */
/*   /1* RFIDSend(1); *1/ */
/*   /1* buzz(); *1/ */
/*   delay(50); */
/*   // sendData(RFIDBuff); */
/*   /1* delay(5000); *1/ */
/* } */

// void loop() {
//   // while (RFID1.available()) {
//   //   D(Serial.print(RFID1.read()));
//   // }
//   // D(Serial.println());
//   if (RFID1.available()) {
//     D(Serial.print("RFID1: "));
//     RFID1.readBytes(RFIDBuff, 12);
//     RFIDSend(1);
//     buzz();
//     delay(500);
//     // D(Serial.println(RFIDBuff));
//     // sendData(RFIDBuff);
//   }
//   if (RFID2.available()) {
//     D(Serial.print("RFID2: "));
//     RFID2.readBytes(RFIDBuff, 12);
//     RFIDSend(2);
//     buzz();
//     delay(500);
//     // D(Serial.println(RFIDBuff));
//   }
//   // while (RFID2.available()) {
//   //   D(Serial.print(RFID2.read()));
//   // }
//   // if (RFID2.available()) {
//   //   RFID2.readBytes(RFIDBuff, 12);
//   //   D(Serial.println(RFIDBuff));
//   // }
// }

void buzz() {
  digitalWrite(BUZZER, 1);
  delay(30);
  digitalWrite(BUZZER, 0);
}

void RFIDSend(int rfidNumber) {
  char message[200] = {0};
  // connectServer();
  endData();
  startData();
  delay(100);
  GSM.print('$');
  sendLocation();
  GSM.print(',');
  GSM.print(RFIDBuff);
  GSM.print(',');
  switch (rfidNumber) {
  case 1:
    // strcat(RFIDBuff, ",1");
    RFIDBuff[12] = ',';
    RFIDBuff[13] = '1';
    RFIDBuff[14] = '\0';
    sendWeight(1);
    break;
  case 2:
    // strcat(RFIDBuff, ",2");
    RFIDBuff[12] = ',';
    RFIDBuff[13] = '2';
    RFIDBuff[14] = '\0';
    sendWeight(2);
    break;
  }
  GSM.print('#');
  endData();
  // strcpy(message, "$");
  // strcat(message, getLocation());
  // strcat(message, ",");
  // strcat(message, RFIDBuff);
  // strcat(message, ",");
  // strcat(message, getWeight(rfidNumber));
  // strcat(message, "#");
  // sendData(message);
  // RFIDBuff[12] = '\0';
}
