// #include <GPSport.h>
// #include <NMEAGPS.h>
// #include <StaticThreadController.h>
// #include <Thread.h>
// #include <ThreadController.h>

// #include <Time.h>
// #include <timestamp32bits.h>

// #include <EEPROM.h>
// #include <string.h>

// // #define gpsPort Serial1
// #define GSM Serial3
// #define EEPROMAddr 0

// struct EEPROMStruct {
//   bool GSMResetFlag;
//   char IMEI[20];
//   char APN[20];
//   char IP[20];
//   char port[10];
//   int delaytime;
// };

// EEPROMStruct Settings;

// static NMEAGPS gps; // This parses the GPS characters
// static gps_fix fix; // This holds on to the latest values

// timestamp32bits stamp = timestamp32bits();

// bool unreadSMSFlag = false;
// bool serverConnected = false;

// char *RPC = NULL;

// // ThreadController that will controll all threads
// ThreadController controll = ThreadController();

// // Thread to handle incoming SMSes
// Thread SMSThread = Thread();
// Thread statusThread = Thread();
// Thread RPCThread = Thread();

// void setup() {
//   D(Serial.println("# in setup()"));
//   Serial.begin(9600);
//   while (!Serial)
//     ;
//   Serial.println(F("Setup: started"));

//   gpsPort.begin(9600);

//   GSM.begin(9600);
//   //   // gpsPort.begin(9600);
//   //   // while (!gpsPort)
//   //     // ;
//   EEPROM.get(EEPROMAddr, Settings);
//   resetGSMIfNecessary();
//   // resetGSM();
//   initGSM();
// updateIMEI();
//   initGPRS();
//   connectServer();
//   deleteAllMessages();
//   initThread(&SMSThread, unreadSMSHandler, 10000);
//   initThread(&statusThread, statusHandler, 100);
//   // initThread(&RPCThread, RPCHandler, 1000);
//   // RPCThread.enabled = false;
//   // setTime(getTimeFromGSM());

//   // Serial.println(getTimeFromGSM())
//   Serial.println(F("Setup: done"));
// }

// void initThread(Thread *a, void (*ThreadHandler)(void), int interval) {
//   Serial.println("# in initThread(Thread");
//   a->onRun(ThreadHandler);
//   a->setInterval(interval);
//   controll.add(a);
// }

// void loop() {
//   controll.run();
//   // Serial.println(F("Looping"));
//   // controll.run();
//   // printLoc();
//   //   readToGSMBuffer();
//   //   updateIP("reisub.ddns.net");
//   delay(Settings.delaytime);
// }

// void statusHandler() {
//   Serial.println("# in statusHandler()");
//   tryUntilResponse("AT", "OK");
//   updateLocation();
//   // printLocation();
//   connectServer();
//   char *data = getLocation();
//   sendData(data);
//   free(data);
//   disconnectServer();
// }

void updateIMEI() {
  Serial.println("# in updateIMEI()");
  char *actual = getIMEI();
  if (strcmp(actual, Settings.IMEI) != 0) {
    strcpy(Settings.IMEI, actual);
    EEPROM.put(EEPROMAddr, Settings);
  }
  free(actual);
}

void updateAPN(char *newAPN) {
  Serial.println("# in updateAPN(char");
  strcpy(Settings.APN, newAPN);
  EEPROM.put(EEPROMAddr, Settings);
}

void updateIP(char *newIP) {
  Serial.println("# in updateIP(char");
  strcpy(Settings.IP, newIP);
  EEPROM.put(EEPROMAddr, Settings);
}

void updatePort(char *newPort) {
  Serial.println("# in updatePort(char");
  strcpy(Settings.port, newPort);
  EEPROM.put(EEPROMAddr, Settings);
}

void updateDelaytime(int newDelay) {
  Serial.println("# in updateDelaytime(int");
  Settings.delaytime = newDelay * 1000;
  EEPROM.put(EEPROMAddr, Settings);
}

// void unreadSMSHandler() {
//   Serial.println("# in unreadSMSHandler()");
//   if (unreadSMSFlag) {
//     Serial.println(F("Unread SMS Detected! Running SMS Code"));
//     // Guaranteed that the message will be at position 1
//     readMessage();

//     // Parse and process SMS (Execute action depending on command)
//     processSMS();

//     deleteAllMessages();

//     unreadSMSFlag = false;
//   }
// }

// void RPCHandler() {
//   Serial.println("# in RPCHandler()");
//   if (RPC != NULL) {
//     processRPC();
//     free(RPC);
//     RPC = NULL;
//   }
// }

void resetGSMIfNecessary() {
  D(Serial.println("# in resetGSMIfNecessary()"));
  if (Settings.GSMResetFlag) {
    D(Serial.println("Reset flag was set! Resetting GSM"));
    Settings.GSMResetFlag = false;
    EEPROM.put(EEPROMAddr, Settings);
    resetGSM();
  }
}

// // Process the SMS That is in the GSMBuffer
// void processRPC() {
//   Serial.println("# in processRPC()");
//   Serial.print(F("processRPC received: "));
//   Serial.println(RPC);
//   if (strstr(RPC, "*") == NULL) {
//     Serial.print(F("Invalid message on RPC"));
//     return;
//   }
//   if (strstr(RPC, "#") == NULL) {
//     Serial.print(F("Invalid message on RPC"));
//     return;
//   }
//   // char *dropped = strtok(RPC, "*");

//   // char *number = strtok(NULL, ",");
//   // Serial.print(F("number = "));
//   // Serial.println(number);

//   // dropped = strtok(NULL, "*");

//   char *prefix = strtok(RPC, " ");
//   Serial.print(F("prefix = "));
//   Serial.println(prefix);

//   if (strcmpi(prefix, "rpc") != 0) {
//     return;
//   } else {
//     char *command = strtok(NULL, "#");
//     if (command == NULL) {
//       return;
//     }
//     Serial.print(F("command = "));
//     Serial.println(command);

//     if (strcmpi(command, "reset") == 0) {
//       Serial.println(F("RESET MESSAGE RECEIVED"));

//       SMSHandlerReset();
//     }
//   }
//   Serial.println(F("ProcessRPC Done..."));
// }

void (*resetFunc)(void) = 0;

void SMSHandlerReset() {
  D(Serial.println("# in SMSHandlerReset()"));
  Settings.GSMResetFlag = true;
  EEPROM.put(EEPROMAddr, Settings);
  // resetGSM();
  resetFunc();
}
