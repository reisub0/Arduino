/*
 * https://circuits4you.com
 * 2016 November 25
 * Load Cell HX711 Module Interface with Arduino to measure weight in Kgs
 Arduino
 pin
 2 -> HX711 CLK
 3 -> DOUT
 5V -> VCC
 GND -> GND

 Most any pin on the Arduino Uno will be compatible with DOUT/CLK.
 The HX711 board can be powered from 2.7V to 5V so the Arduino 5V power should
 be fine.
*/

#include "HX711.h" //You must have this library in your arduino library folder

#define DDOUT A0
#define DCLK A1
#define WDOUT A2
#define WCLK A3
#define scale Wetscale

HX711 Dryscale(DDOUT, DCLK);
HX711 Wetscale(WDOUT, WCLK);

// Change this calibration factor as per your load cell once it is found you
// many need to vary it in thousands
float calibration_factor = -106600; //-106600 worked for my 40Kg max scale setup

void loadSetup() {
  /* Dryscale.set_scale(); */
  Dryscale.set_scale(calibration_factor);
  Dryscale.tare();                            // Reset the Dryscale to 0
  long zero_factor = Dryscale.read_average(); // Get a baseline reading
  /* Wetscale.set_scale(); */
  Wetscale.set_scale(calibration_factor);
  Wetscale.tare(); // Reset the Dryscale to 0
}

char *getWeight(int scaleNo) {
  char *weight = malloc(15);
  if (scaleNo == 1) {
    Dryscale.set_scale(calibration_factor);
    dtostrf(Dryscale.get_units(), 3, 3, weight);
    return weight;
  } else if (scaleNo == 2) {
    Wetscale.set_scale(calibration_factor);
    dtostrf(Wetscale.get_units(20), 3, 3, weight);
    return weight;
  }
}

void sendWeight(int scaleNo) {
  if (scaleNo == 1) {
    Dryscale.set_scale(calibration_factor);
    GSM.print(Dryscale.get_units(), 3);
  } else if (scaleNo == 2) {
    Wetscale.set_scale(calibration_factor);
    GSM.print(Wetscale.get_units(), 3);
  }
}

//=============================================================================================
//                         SETUP
//=============================================================================================
 void setup() {
  Serial.begin(9600);
  D(Serial.println("HX711 Calibration"));
  D(Serial.println("Remove all weight from scale"));
  D(Serial.println("After readings begin, place known weight on scale"));
  D(Serial.println("Press a,s,d,f to increase calibration factor by ) 10,100,1000,10000 respectively"));
  D(Serial.println("Press z,x,c,v to decrease calibration factor by ) 10,100,1000,10000 respectively"));
  D(Serial.println("Press t for tare"));
  scale.set_scale();
  scale.tare(); // Reset the scale to 0

  long zero_factor = scale.read_average(); // Get a baseline reading
  D(Serial.print( "Zero factor: ")); // This can be used to remove the need to tare the
                        // scale. Useful in permanent scale projects.
  D(Serial.println(zero_factor));
}

////=============================================================================================
////                         LOOP
////=============================================================================================
 void loop() {

   scale.set_scale(calibration_factor); // Adjust to this calibration factor

   D(Serial.print("Reading: "));
   D(Serial.print(scale.get_units(5), 3));
   D(Serial.print(" kg")); // Change this to kg and re-adjust the calibratio)n
                        // factor if you follow SI units like a sane person
   D(Serial.print(" calibration_factor: "));
   D(Serial.print(calibration_factor));
   D(Serial.println());

   if (Serial.available()) {
     char temp = Serial.read();
     if (temp == '+' || temp == 'a')
       calibration_factor += 10;
     else if (temp == '-' || temp == 'z')
       calibration_factor -= 10;
     else if (temp == 's')
       calibration_factor += 100;
     else if (temp == 'x')
       calibration_factor -= 100;
     else if (temp == 'd')
       calibration_factor += 1000;
     else if (temp == 'c')
       calibration_factor -= 1000;
     else if (temp == 'f')
       calibration_factor += 10000;
     else if (temp == 'v')
       calibration_factor -= 10000;
     else if (temp == 't')
       scale.tare(); // Reset the scale to zero
   }
 }
////=============================================================================================
