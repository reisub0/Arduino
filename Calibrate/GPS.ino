// #undef DAYS_PER_WEEK
#define gpsPort Serial1
#define GPS_PORT_NAME "Serial1"
#define DEBUG_PORT Serial
#include <NMEAGPS.h>
#include <Time.h>

void sendLocation() {
  updateLocation();

  GSM.print(Settings.IMEI);
  GSM.print(',');

  GSM.print(fix.dateTime.year);
  GSM.print('/');
  if (fix.dateTime.month < 10)
    GSM.print('0');
  GSM.print(fix.dateTime.month);
  GSM.print('/');
  if (fix.dateTime.date < 10)
    GSM.print('0');
  GSM.print(fix.dateTime.date);
  GSM.print(',');

  if (fix.dateTime.hours < 10)
    GSM.print('0');
  GSM.print(fix.dateTime.hours);
  GSM.print(':');
  if (fix.dateTime.minutes < 10)
    GSM.print('0');
  GSM.print(fix.dateTime.minutes);
  GSM.print(':');
  if (fix.dateTime.seconds < 10)
    GSM.print('0');
  GSM.print(fix.dateTime.seconds);
  GSM.print("+22");
  GSM.print(',');

  GSM.print(fix.latitude(), 6);
  GSM.print(',');

  GSM.print(fix.longitude(), 6);
}

//--------------------------
// char *getLocation() {
//   updateLocation();
//   char *message = calloc(1, 200);
//   char *buff = malloc(15);

//   // strcpy(message, "$");
//   strcat(message, Settings.IMEI);
//   strcat(message, ",");

//   // ltoa(year(), buff, 10);
//   // strcat(message, buff);
//   // strcat(message, "/");
//   // ltoa(month(), buff, 10);
//   // strcat(message, buff);
//   // strcat(message, "/");
//   // ltoa(day(), buff, 10);
//   // strcat(message, buff);
//   // strcat(message, ",");
//   // ltoa(hour(), buff, 10);
//   // strcat(message, buff);
//   // strcat(message, ":");
//   // ltoa(minute(), buff, 10);
//   // strcat(message, buff);
//   // strcat(message, ":");
//   // ltoa(second(), buff, 10);
//   // strcat(message, buff);
//   // ltoa(nowTime.year, buff, 10);
//   // strcat(message, buff);
//   // strcat(message, buff);
//   ltoa(fix.dateTime.year, buff, 10);
//   strcat(message, buff);
//   strcat(message, "/");
//   ltoa(fix.dateTime.month, buff, 10);
//   strcat(message, buff);
//   strcat(message, "/");
//   ltoa(fix.dateTime.date, buff, 10);
//   strcat(message, buff);
//   strcat(message, ",");
//   ltoa(fix.dateTime.hours, buff, 10);
//   strcat(message, buff);
//   strcat(message, ":");
//   ltoa(fix.dateTime.minutes, buff, 10);
//   strcat(message, buff);
//   strcat(message, ":");
//   ltoa(fix.dateTime.seconds, buff, 10);
//   strcat(message, buff);
//   strcat(message, ",");

//   dtostrf(fix.latitude(), 8, 6, buff);
//   strcat(message, buff);
//   strcat(message, ",");

//   dtostrf(fix.longitude(), 8, 6, buff);
//   strcat(message, buff);
//   // strcat(message, "#");

//   D(Serial.print(F("Got Location: ")));
//   D(Serial.print(message));
//   D(Serial.println());

//   free(buff);

//   return message;
// }

void printLocation() {
  // if (fix.valid.location) {
  D(Serial.print(F("Location: ")));
  // } else
  // D(Serial.print(F("-")));

  // // if (fix.valid.altitude) {
  // D(Serial.print(F(", Altitude: ")));
  // D(Serial.print(fix.altitude()));
  // // } else
  // D(Serial.print(F("-")));
  // while (!gps.available(gpsPort))
  //   ;
  // while (gps.available(gpsPort)) {
  //   fix = gps.read();
  // }

  // if (fix.valid.location) {
  //   D(Serial.print(F("Location: ")));
  D(Serial.print(fix.latitude(), 6));
  D(Serial.print(','));
  D(Serial.print(fix.longitude(), 6));
  // } else
  //   D(Serial.print(F("-")));

  // if (fix.valid.altitude) {
  //   D(Serial.print(F(", Altitude: ")));
  //   D(Serial.print(fix.altitude()));
  // } else
  //   D(Serial.print(F("-")));

  D(Serial.println());
}

void updateLocation() {
  while (!gps.available(gpsPort))
    ;
  while (gps.available(gpsPort)) {
    fix = gps.read();
  }
}

int getNoSatellites() {
  // while (!gps.available(gpsPort))
  //   ;
  // while (gps.available(gpsPort)) {
  //   fix = gps.read();
  // }
  return gps.sat_count;
}
