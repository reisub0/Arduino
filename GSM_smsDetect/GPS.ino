#undef DAYS_PER_WEEK
#define gpsPort Serial1
#define GPS_PORT_NAME "Serial1"
#define DEBUG_PORT Serial
#include <NMEAGPS.h>

//--------------------------
char *getLocation() {
  char *message = calloc(1, 200);
  char *buff = malloc(15);

  strcpy(message, "$");
  strcat(message, Settings.IMEI);
  strcat(message, ",");

  char *newbuff = getGSMTimestamp();
  strcat(message, newbuff);
  free(newbuff);
  strcat(message, ",");

  dtostrf(fix.latitude(), 8, 6, buff);
  strcat(message, buff);
  strcat(message, ",");

  dtostrf(fix.longitude(), 8, 6, buff);
  strcat(message, buff);
  strcat(message, "#");

  Serial.print(F("Got Location: "));
  Serial.print(message);
  Serial.println();

  free(buff);

  return message;
}

void printLocation() {
  // if (fix.valid.location) {
  Serial.print(F("Location: "));
  // } else
  // Serial.print(F("-"));

  // // if (fix.valid.altitude) {
  // Serial.print(F(", Altitude: "));
  // Serial.print(fix.altitude());
  // // } else
  // Serial.print(F("-"));
  // while (!gps.available(gpsPort))
  //   ;
  // while (gps.available(gpsPort)) {
  //   fix = gps.read();
  // }

  // if (fix.valid.location) {
  //   Serial.print(F("Location: "));
  Serial.print(fix.latitude(), 6);
  Serial.print(',');
  Serial.print(fix.longitude(), 6);
  // } else
  //   Serial.print(F("-"));

  // if (fix.valid.altitude) {
  //   Serial.print(F(", Altitude: "));
  //   Serial.print(fix.altitude());
  // } else
  //   Serial.print(F("-"));

  Serial.println();
}

void updateLocation() {
  while (!gps.available(gpsPort))
    ;
  while (gps.available(gpsPort)) {
    fix = gps.read();
  }
}

int getNoSatellites() {
  // while (!gps.available(gpsPort))
  //   ;
  // while (gps.available(gpsPort)) {
  //   fix = gps.read();
  // }
  return gps.sat_count;
}
