#include <GPSport.h>
#include <NMEAGPS.h>
#include <StaticThreadController.h>
#include <Thread.h>
#include <ThreadController.h>

#include <Time.h>
#include <timestamp32bits.h>

#include <EEPROM.h>
#include <string.h>

// #define gpsPort Serial1
#define GSM Serial3
#define EEPROMAddr 0
#define BUFSIZE 200

struct EEPROMStruct {
  bool GSMResetFlag;
  char IMEI[20];
  char APN[20];
  char IP[20];
  char port[10];
};

EEPROMStruct Settings;

static NMEAGPS gps; // This parses the GPS characters
static gps_fix fix; // This holds on to the latest values

timestamp32bits stamp = timestamp32bits();

const char SERVERCONFIG[] PROGMEM =
    "AT+CIPSTART=\"TCP\",\"reisub.ddns.net\",\"1234\"\r";

bool unreadSMSFlag = false;

char GSMBuffer[BUFSIZE];
char *RPC = NULL;

// ThreadController that will controll all threads
ThreadController controll = ThreadController();

// Thread to handle incoming SMSes
Thread SMSThread = Thread();
Thread statusThread = Thread();
Thread RPCThread = Thread();

void setup() {
  Serial.begin(9600);
  while (!Serial)
    ;
  Serial.println(F("Setup: started"));

  gpsPort.begin(9600);

  GSM.begin(9600);
  //   // gpsPort.begin(9600);
  //   // while (!gpsPort)
  //     // ;
  EEPROM.get(EEPROMAddr, Settings);
  resetGSMIfNecessary();
  updateIMEI();
  // resetGSM();
  // initGSM();
  // initGPRS();
  // connectServer();
  deleteAllMessages();
  initThread(&SMSThread, unreadSMSHandler, 10000);
  initThread(&statusThread, statusHandler, 10000);
  initThread(&RPCThread, RPCHandler, 1000);
  // RPCThread.enabled = false;
  setTime(getTimeFromGSM());

  // Serial.println(getTimeFromGSM())
  Serial.println(F("Setup: done"));
}

void initThread(Thread *a, void (*ThreadHandler)(void), int interval) {
  a->onRun(ThreadHandler);
  a->setInterval(interval);
  controll.add(a);
}

void loop() {
  controll.run();
  // Serial.println(F("Looping"));
  // controll.run();
  // printLoc();
  //   // controll.run();
  //   while (Serial.available()) {
  //   Serial3.write(Serial.read());
  //   if (Serial.find("STOP")) {
  //        while (1)
  //         ;
  //     }
  //   }
  //   readToGSMBuffer();
  //   updateIP("reisub.ddns.net");
  // delay(5000);
  //   // printLoc();
  //   Serial.print("Flag = ");
  //   Serial.println(unreadSMSFlag);
}

void statusHandler() {
  tryUntilResponse("AT", "OK");
  updateLoc();
  printLoc();
}

void unreadSMSHandler() {
  if (unreadSMSFlag) {
    Serial.println(F("Unread SMS Detected! Running SMS Code"));
    // Guaranteed that the message will be at position 1
    readMessage();

    // Parse and process SMS (Execute action depending on command)
    processSMS();

    deleteAllMessages();

    unreadSMSFlag = false;
  }
}

void RPCHandler() {
  if (RPC != NULL) {
    processRPC();
    free(RPC);
    RPC = NULL;
  }
}

void resetGSMIfNecessary() {
  if (Settings.GSMResetFlag) {
    Serial.println("Reset flag was set! Resetting GSM");
    Settings.GSMResetFlag = false;
    EEPROM.put(EEPROMAddr, Settings);
    resetGSM();
  }
}

// Process the SMS That is in the GSMBuffer
void processRPC() {
  Serial.print(F("processRPC received: "));
  Serial.println(RPC);
  if (strstr(RPC, "*") == NULL) {
    Serial.print(F("Invalid message on RPC"));
    return;
  }
  if (strstr(RPC, "#") == NULL) {
    Serial.print(F("Invalid message on RPC"));
    return;
  }
  // char *dropped = strtok(RPC, "*");

  // char *number = strtok(NULL, ",");
  // Serial.print(F("number = "));
  // Serial.println(number);

  // dropped = strtok(NULL, "*");

  char *prefix = strtok(RPC, " ");
  Serial.print(F("prefix = "));
  Serial.println(prefix);

  if (strcmpi(prefix, "rpc") != 0) {
    return;
  } else {
    char *command = strtok(NULL, "#");
    if (command == NULL) {
      return;
    }
    Serial.print(F("command = "));
    Serial.println(command);

    if (strcmpi(command, "reset") == 0) {
      Serial.println(F("RESET MESSAGE RECEIVED"));

      SMSHandlerReset();
    }
  }
  Serial.println(F("ProcessRPC Done..."));
}

// Process the SMS That is in the GSMBuffer
void processSMS() {
  Serial.print(F("processSMS received: "));
  Serial.println(GSMBuffer);
  char *dupBuffer = strdup(GSMBuffer);

  char *dropped = strtok(dupBuffer, ",");

  char *number = strtok(NULL, ",");
  Serial.print(F("number = "));
  Serial.println(number);

  dropped = strtok(NULL, "*");

  char *company = strtok(NULL, " ");
  Serial.print(F("company = "));
  Serial.println(company);

  if (strcmpi(company, "ava") != 0) {
    return;
  } else {
    char *command = strtok(NULL, "#");
    if (command == NULL) {
      return;
    }
    Serial.print(F("command = "));
    Serial.println(command);

    if (strcmpi(command, "reset") == 0) {
      Serial.println(F("RESET MESSAGE RECEIVED"));

      char *message = "Resetted the system";
      sendMessage(message, number);

      SMSHandlerReset();
    } else if (strcmpi(command, "imei") == 0) {
      Serial.println(F("IMEI MESSAGE RECEIVED"));
      char message[50] = "MY IMEI IS: ";
      strcat(message, Settings.IMEI);
      sendMessage(message, number);
    } else if (strcmpi(command, "rpc") == 0) {
      Serial.println(F("COMMAND MESSAGE RECEIVED"));
      char message[100] = "TRYING TO ACTIVATE COMMAND MODE AT: ";
      strcat(message, Settings.IP);
      strcat(message, ":");
      strcat(message, Settings.port);
      sendMessage(message, number);
      connectServer();
      time_t commandStartTime = now();
    } else {
      char *word1 = strtok(command, " ");
      if (word1 == NULL) {
        return;
      }
      if (strcmpi(word1, "apn") == 0) {
        char *word2 = strtok(NULL, "#");
        if (word2 == NULL) {
          return;
        }
        Serial.println(F("APN MESSAGE RECEIVED"));
        char *prevAPN = strdup(Settings.APN);
        char message[100] = "MY PREVIOUS APN WAS: ";
        strcat(message, prevAPN);
        strcat(message, "\nUpdating to: ");
        strcat(message, word2);
        updateAPN(word2);
        sendMessage(message, number);
        Serial.println("Updated apn successfully");
      } else if (strcmpi(word1, "ip") == 0) {
        char *word2 = strtok(NULL, "#");
        if (word2 == NULL) {
          return;
        }
        Serial.println(F("IP MESSAGE RECEIVED"));
        char *prevIP = strdup(Settings.IP);
        char message[100] = "MY PREVIOUS IP WAS: ";
        strcat(message, prevIP);
        strcat(message, "\nUpdating to: ");
        strcat(message, word2);
        updateIP(word2);
        sendMessage(message, number);
        Serial.println("Updated IP successfully");
      } else if (strcmpi(word1, "port") == 0) {
        char *word2 = strtok(NULL, "#");
        if (word2 == NULL) {
          return;
        }
        Serial.println(F("PORT MESSAGE RECEIVED"));
        char *prevPort = strdup(Settings.port);
        char message[100] = "MY PREVIOUS PORT WAS: ";
        strcat(message, prevPort);
        strcat(message, "\nUpdating to: ");
        strcat(message, word2);
        updatePort(word2);
        sendMessage(message, number);
        Serial.println("Updated PORT successfully");
      }
    }
  }
  Serial.println(F("ProcessSMS Done..."));
}

void (*resetFunc)(void) = 0;

void SMSHandlerReset() {
  Settings.GSMResetFlag = true;
  EEPROM.put(EEPROMAddr, Settings);
  // resetGSM();
  resetFunc();
}
