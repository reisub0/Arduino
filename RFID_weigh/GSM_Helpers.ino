#define BUFSIZE 200
char GSMBuffer[BUFSIZE];

// Process the SMS That is in the GSMBuffer
void processSMS() {
  Serial.println("# in processSMS()");
  Serial.print(F("processSMS received: "));
  Serial.println(GSMBuffer);
  char *dupBuffer = strdup(GSMBuffer);

  char *dropped = strtok(dupBuffer, ",");

  char *number = strtok(NULL, ",");
  Serial.print(F("number = "));
  Serial.println(number);

  dropped = strtok(NULL, "*");

  char *company = strtok(NULL, " ");
  Serial.print(F("company = "));
  Serial.println(company);

  if (strcmpi(company, "ava") != 0) {
    return;
  } else {
    char *command = strtok(NULL, "#");
    if (command == NULL) {
      return;
    }
    Serial.print(F("command = "));
    Serial.println(command);

    if (strcmpi(command, "reset") == 0) {
      Serial.println(F("RESET MESSAGE RECEIVED"));

      char *message = "Resetted the system";
      sendMessage(message, number);

      SMSHandlerReset();
    } else if (strcmpi(command, "imei") == 0) {
      Serial.println(F("IMEI MESSAGE RECEIVED"));
      char message[50] = "MY IMEI IS: ";
      strcat(message, Settings.IMEI);
      sendMessage(message, number);
      // } else if (strcmpi(command, "rpc") == 0) {
      //   Serial.println(F("RPC MESSAGE RECEIVED"));
      //   char message[100] = "TRYING TO ACTIVATE RPC MODE AT: ";
      //   strcat(message, Settings.IP);
      //   strcat(message, ":");
      //   strcat(message, Settings.port);
      //   sendMessage(message, number);
      //   connectServer();
      //   // time_t commandStartTime = now();
    } else if (strcmpi(command, "status") == 0) {
      Serial.println(F("STATUS MESSAGE RECEIVED"));
      char message[200] = "GSM Status->\n";
      strcat(message, "IMEI: ");
      strcat(message, Settings.IMEI);
      strcat(message, "\n");
      strcat(message, "SIGNAL: ");
      strcat(message, getStrength());
      strcat(message, "\n");
      strcat(message, "APN: ");
      strcat(message, Settings.APN);
      strcat(message, "\n");
      strcat(message, "SERVER: ");
      strcat(message, Settings.IP);
      strcat(message, ":");
      strcat(message, Settings.port);
      strcat(message, "\n");
      strcat(message, "GPS Status->\n");
      char *buff = malloc(10);
      strcat(message, "Current DELAY TIME->");
      strcat(message, itoa(Settings.delaytime / 1000, buff, 10));
      free(buff);
      strcat(message, "\n");
      sendMessage(message, number);
    } else {
      char *word1 = strtok(command, " ");
      if (word1 == NULL) {
        return;
      }
      if (strcmpi(word1, "apn") == 0) {
        char *word2 = strtok(NULL, "#");
        if (word2 == NULL) {
          return;
        }
        Serial.println(F("APN MESSAGE RECEIVED"));
        char *prevAPN = strdup(Settings.APN);
        char message[100] = "MY PREVIOUS APN WAS: ";
        strcat(message, prevAPN);
        strcat(message, "\nUpdating to: ");
        strcat(message, word2);
        updateAPN(word2);
        sendMessage(message, number);
        Serial.println("Updated apn successfully");
      } else if (strcmpi(word1, "ip") == 0) {
        char *word2 = strtok(NULL, "#");
        if (word2 == NULL) {
          return;
        }
        Serial.println(F("IP MESSAGE RECEIVED"));
        char *prevIP = strdup(Settings.IP);
        char message[100] = "MY PREVIOUS IP WAS: ";
        strcat(message, prevIP);
        strcat(message, "\nUpdating to: ");
        strcat(message, word2);
        updateIP(word2);
        sendMessage(message, number);
        Serial.println("Updated IP successfully");
      } else if (strcmpi(word1, "port") == 0) {
        char *word2 = strtok(NULL, "#");
        if (word2 == NULL) {
          return;
        }
        Serial.println(F("PORT MESSAGE RECEIVED"));
        char *prevPort = strdup(Settings.port);
        char message[100] = "MY PREVIOUS PORT WAS: ";
        strcat(message, prevPort);
        strcat(message, "\nUpdating to: ");
        strcat(message, word2);
        updatePort(word2);
        sendMessage(message, number);
        Serial.println("Updated PORT successfully");
      } else if (strcmpi(word1, "delay") == 0) {
        char *word2 = strtok(NULL, "#");
        if (word2 == NULL) {
          return;
        }
        Serial.println(F("DELAY MESSAGE RECEIVED"));
        int prevDelaytime = Settings.delaytime;
        char message[100] = "MY PREVIOUS DELAY WAS: ";
        char *buff = malloc(10);
        strcat(message, itoa(Settings.delaytime / 1000, buff, 10));
        free(buff);
        strcat(message, "\nUpdating to: ");
        strcat(message, word2);
        updateDelaytime(atoi(word2));
        sendMessage(message, number);
        Serial.println("Updated DELAY successfully");
      }
    }
  }
  Serial.println(F("ProcessSMS Done..."));
}

void resetGSM() {
  Serial.println("# in resetGSM()");
  tryUntilResponse("AT+CFUN=1,1", "OK");
  initGSM();
}

void initGSM() {
  Serial.println("# in initGSM()");
  // Reset the module first */
  // tryUntilResponse("AT+CFUN=1,1", "OK"); */
  // Give it some time to start up */
  // delay(10000);
  tryUntilResponse("ATE0", "OK");
  tryUntilResponse("AT+CNMI=1,1,0,0,0", "OK");
  tryUntilResponse("AT+CLTS=1", "OK");
  // tryUntilResponse("AT+COPS=2", "OK"); */
  // tryUntilResponse("AT+COPS=0", "OK"); */
  tryUntilResponse("AT+CMGF=1", "OK");
  tryUntilResponse("AT&W", "OK");

  tryUntilResponse("AT+CPIN?", "READY");
  // Ensure text mode is set for SMSes */
  tryUntilResponse("AT+CMGF=1", "OK");
  // Delete all messages */
  tryUntilResponse("AT+CMGD=1,4", "OK");
  // tryUntilResponse("AT", "OK"); */
  // tryUntilResponse("ATE0", "OK"); */
  // tryUntilResponse("AT+CPIN?", "READY"); */
  // tryUntilResponse("AT+CLTS=1", "OK"); */
  // tryUntilResponse("AT+COPS=2", "OK"); */
  // tryUntilResponse("AT+COPS=0", "OK"); */
  // tryUntilResponse("AT&W", "OK"); */
  Serial.println(F("Done initing GSM"));
}

void initGPRS() {
  Serial.println("# in initGPRS()");
  tryUntilResponse("AT+CIPSHUT", "OK");
  tryUntilResponse("AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\"\r", "OK");
  tryUntilResponse("AT+SAPBR=3,1,\"APN\",\"airtelgprs.com\"\r", "OK");
  putCMD("AT+SAPBR=1,1");
  delay(1000);

  tryUntilResponse("AT+CIPMUX=0", "OK");
  delay(5000);
  Serial.println(F("Done initing GPRS"));
}

void disconnectServer() {
  Serial.println("# in disconnectServer()");
  tryUntilResponse("AT+CIPSHUT", "OK");
}

void connectServer() {
  Serial.println("# in connectServer()");
  tryUntilResponse("AT+CIPSHUT", "OK");
  char message[100] = "AT+CIPSTART=\"TCP\",";
  strcat(message, "\"");
  strcat(message, Settings.IP);
  strcat(message, "\",\"");
  strcat(message, Settings.port);
  strcat(message, "\"");

  for (int i = 0; i < 3; i++) {
    putCMD(message);
    readToGSMBuffer();
    if (strstr(GSMBuffer, "FAIL") > 0) {
      delay(2000);
      continue;
    } else if (strstr(GSMBuffer, "ALREADY") > 0) {
      return;
    }
  }
}

void putCMD(char *cmd) {
  GSM.print(cmd);
  GSM.write('\r');
  GSM.write('\n');
  // Serial.print(F("Sent command: ")); */
  // Serial.println(cmd); */
}

void readToGSMBuffer() {
  String resp = "";
  while (GSM.available() == 0)
    ;
  while (GSM.available() > 0) {
    delay(1);
    resp = resp + GSM.readString();
  }
  // if (resp != "") {
  //   Serial.print(F("Got: \""));
  //   Serial.print(resp);
  //   Serial.println(F("\""));
  // }
  if (resp.indexOf("+CMTI") >= 0) {
    unreadSMSFlag = true;
    // resp = ""; */
    // GSMBuffer[0] = '\0'; */
  }
  resp.toCharArray(GSMBuffer, BUFSIZE);
  if (resp.indexOf("*rpc") >= 0) {
    RPC = strdup(GSMBuffer);
  }
}

// Helper function that returns the string that the GSM Module responded with */
void getGSMResponse(char *cmd) {
  putCMD(cmd);
  readToGSMBuffer();
}

// Keep trying a command until the response matches with waitTime of x ms */
void tryUntilResponse(char *cmd, char *res) {
  GSM.flush();
  putCMD("");
  while (1) {
    getGSMResponse(cmd);
    if (strstr(GSMBuffer, res) != NULL) {
      return;
    }
    // delay(waitTime); */
    delay(1000);
    // Serial.flush(); */
    // GSM.flush(); */
  }
}

void readMessage() {
  Serial.println("# in readMessage()");
  getGSMResponse("AT+CMGR=1");
}

void startSMS(char *number) {
  Serial.println("# in startSMS(char");
  char command[30] = {0};
  strcpy(command, "AT+CMGS=");
  strcat(command, number);
  putCMD(command);
}

void startData() {
  Serial.println("# in startData()");
  char command[30] = {0};
  strcpy(command, "AT+CIPSEND");
  putCMD(command);
}

void endSMS() { GSM.println((char)26); }
void endData() { GSM.println((char)26); }

void sendMessage(char *text, char *number) {
  Serial.println("# in sendMessage(char");
  char command[300] = {0};
  startSMS(number);
  delay(100);
  putCMD(text);
  delay(100);
  endSMS();
  Serial.print("Sending message to number");
  Serial.print(text);
  Serial.print(':');
  Serial.println(number);
  // GSM.println((char)26); */
  // tryUntilResponse("AT", "OK"); */
  delay(1000);
  // readToGSMBuffer(); */
}

void sendData(char *text) {
  Serial.println("# in sendData");
  startData();
  delay(100);
  putCMD(text);
  delay(100);
  endData();
  Serial.print("Sending data");
  Serial.print(text);
  delay(1000);
}

void deleteAllMessages() {
  Serial.println("# in deleteAllMessages");
  tryUntilResponse("AT+CMGD=1,4", "OK");
}

char *getGSMTimestamp() {
  Serial.println("# in *getGSMTimestamp()");
  getGSMResponse("AT+CCLK?");
  getGSMResponse("AT+CCLK?");
  delay(50);

  // putCMD("AT+CCLK?");
  // putCMD("AT+CCLK?");
  // GSM.find("\"");
  char *token;
  // Drop all data till the first double quotes */
  token = strtok(GSMBuffer, "\"");

  token = strtok(NULL, "\"");
  char *ts = malloc(20);
  strcpy(ts, token);
  // char *timestamp = strdup(GSMBuffer);
  Serial.println(ts);
  return ts;
}

// unsigned long getTimeFromGSM() {
//   int y, mth, d, h, m, s, adj;

//   // putCMD("AT+CCLK?"); */
//   getGSMResponse("AT+CCLK?");
//   getGSMResponse("AT+CCLK?");
//   delay(50);
//   // y = GSM.parseInt(); */
//   // mth = GSM.parseInt(); */
//   // d = GSM.parseInt(); */
//   // h = GSM.parseInt(); */
//   // m = GSM.parseInt(); */
//   // s = GSM.parseInt(); */
//   // adj = GSM.parseInt(); */

//   char *token;
//   // Drop all data till the first double quotes */
//   token = strtok(GSMBuffer, "\"");
//   // First we get year */
//   token = strtok(NULL, "/");
//   y = atoi(token);

//   token = strtok(NULL, "/");
//   mth = atoi(token);

//   token = strtok(NULL, ",");
//   d = atoi(token);

//   token = strtok(NULL, ":");
//   h = atoi(token);

//   token = strtok(NULL, ":");
//   m = atoi(token);

//   token = strtok(NULL, "+");
//   s = atoi(token);

//   token = strtok(NULL, "\"");
//   adj = atoi(token);

//   // Serial.print(F("year = ")); */
//   // Serial.println(y); */
//   // Serial.print(F("month = ")); */
//   // Serial.println(mth); */
//   // Serial.print(F("date = ")); */
//   // Serial.println(d); */
//   // Serial.print(F("hour = ")); */
//   // Serial.println(h); */
//   // Serial.print(F("minute = ")); */
//   // Serial.println(m); */
//   // Serial.print(F("sec = ")); */
//   // Serial.println(s); */
//   long t = stamp.timestamp(y, mth, d, h, m, s);
//   t -= adj * 15 * 60;
//   return t;
// }

char *getIMEI() {
  Serial.println("# in *getIMEI()");
  getGSMResponse("AT+GSN");
  getGSMResponse("AT+GSN");
  char *imei;

  char *dropped = strtok(GSMBuffer, "\n");
  char *token = strtok(NULL, "\r");
  imei = strdup(token);
  return imei;
}

char *getStrength() {
  Serial.println("# in *getStrength()");
  getGSMResponse("AT+CSQ");
  getGSMResponse("AT+CSQ");
  char *strength;

  char *dropped = strtok(GSMBuffer, "\n");
  char *token = strtok(NULL, "\r");
  strength = strdup(token);
  return strength;
}

int strcmpi(char *s1, char *s2) {
  int i;
  for (i = 0; s1[i] && s2[i]; ++i) {
    if (s1[i] == s2[i] || (s1[i] ^ 32) == s2[i])
      continue;
    else
      break;
  }
  if (s1[i] == s2[i])
    return 0;
  if ((s1[i] | 32) < (s2[i] | 32))
    return -1;
  return 1;
}
