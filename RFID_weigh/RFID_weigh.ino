#include <StaticThreadController.h>
#include <Thread.h>
#include <ThreadController.h>

#include <Time.h>
#include <timestamp32bits.h>

#include <EEPROM.h>
#include <string.h>

#define GSM Serial3
#define RFIDPort Serial1
#define WeighBridgePort Serial2
#define EEPROMAddr 0

String rfidString;
String bridgeString;
int led = 7;

struct EEPROMStruct {
  bool GSMResetFlag;
  char IMEI[20];
  char APN[20];
  char IP[20];
  char port[10];
  int delaytime;
};

EEPROMStruct Settings;

timestamp32bits stamp = timestamp32bits();

bool unreadSMSFlag = false;
bool serverConnected = false;

char *RPC = NULL;

// ThreadController that will controll all threads
ThreadController controll = ThreadController();

// Thread to handle incoming SMSes
Thread SMSThread = Thread();
Thread statusThread = Thread();
Thread RPCThread = Thread();

void setup() {
  Serial.println("# in setup()");
  Serial.begin(9600);
  while (!Serial)
    ;
  Serial.println(F("Setup: started"));

  GSM.begin(9600);
  //   // gpsPort.begin(9600);
  //   // while (!gpsPort)
  //     // ;
  EEPROM.get(EEPROMAddr, Settings);
  resetGSMIfNecessary();
  // resetGSM();
  initGSM();
  updateIMEI();
  initGPRS();
  connectServer();
  deleteAllMessages();
  initThread(&SMSThread, unreadSMSHandler, 10000);
  initThread(&statusThread, statusHandler, 10000);
  initThread(&statusThread, rfidHandler, 10000);
  // initThread(&RPCThread, RPCHandler, 1000);
  // RPCThread.enabled = false;
  // setTime(getTimeFromGSM());

  // Serial.println(getTimeFromGSM())
  Serial.println(F("Setup: done"));
}

void initThread(Thread *a, void (*ThreadHandler)(void), int interval) {
  Serial.println("# in initThread(Thread");
  a->onRun(ThreadHandler);
  a->setInterval(interval);
  controll.add(a);
}

void loop() {
  controll.run();
  // Serial.println(F("Looping"));
  // controll.run();
  // printLoc();
  //   readToGSMBuffer();
  //   updateIP("reisub.ddns.net");
  delay(Settings.delaytime);
}

void statusHandler() {
  Serial.println("# in statusHandler()");
  tryUntilResponse("AT", "OK");
}

void updateIMEI() {
  Serial.println("# in updateIMEI()");
  char *actual = getIMEI();
  if (strcmp(actual, Settings.IMEI) != 0) {
    strcpy(Settings.IMEI, actual);
    EEPROM.put(EEPROMAddr, Settings);
  }
  free(actual);
}

void updateAPN(char *newAPN) {
  Serial.println("# in updateAPN(char");
  strcpy(Settings.APN, newAPN);
  EEPROM.put(EEPROMAddr, Settings);
}

void updateIP(char *newIP) {
  Serial.println("# in updateIP(char");
  strcpy(Settings.IP, newIP);
  EEPROM.put(EEPROMAddr, Settings);
}

void updatePort(char *newPort) {
  Serial.println("# in updatePort(char");
  strcpy(Settings.port, newPort);
  EEPROM.put(EEPROMAddr, Settings);
}

void updateDelaytime(int newDelay) {
  Serial.println("# in updateDelaytime(int");
  Settings.delaytime = newDelay * 1000;
  EEPROM.put(EEPROMAddr, Settings);
}

void unreadSMSHandler() {
  Serial.println("# in unreadSMSHandler()");
  if (unreadSMSFlag) {
    Serial.println(F("Unread SMS Detected! Running SMS Code"));
    // Guaranteed that the message will be at position 1
    readMessage();

    // Parse and process SMS (Execute action depending on command)
    processSMS();

    deleteAllMessages();

    unreadSMSFlag = false;
  }
}

void resetGSMIfNecessary() {
  Serial.println("# in resetGSMIfNecessary()");
  if (Settings.GSMResetFlag) {
    Serial.println("Reset flag was set! Resetting GSM");
    Settings.GSMResetFlag = false;
    EEPROM.put(EEPROMAddr, Settings);
    resetGSM();
  }
}

void (*resetFunc)(void) = 0;

void SMSHandlerReset() {
  Serial.println("# in SMSHandlerReset()");
  Settings.GSMResetFlag = true;
  EEPROM.put(EEPROMAddr, Settings);
  // resetGSM();
  resetFunc();
}

void Data_Send(char *rfidTime, char *bridgeTime) {
  GSM.print("AT+CIPSEND\r");
  Serial.print("AT+CIPSEND\r");

  delay(1000);

  GSM.print(Settings.IMEI);
  Serial.print(Settings.IMEI);

  GSM.write("#");
  Serial.write("#");

  GSM.print(rfidString);
  Serial.print(rfidString);

  GSM.write("#");
  Serial.write("#");

  GSM.print(rfidTime);
  Serial.print(rfidTime);

  GSM.write("#");
  Serial.write("#");

  GSM.print(bridgeString);
  Serial.print(bridgeString);

  GSM.write("#");
  Serial.write("#");

  GSM.print(bridgeTime);
  Serial.print(bridgeTime);

  GSM.write("#");
  Serial.write("#");

  GSM.write("\x1A");
  Serial.write("\x1A");
}

void led_group() {
  int i = 0;
  while (i < 9) {
    digitalWrite(led, HIGH);
    delay(1000);
    digitalWrite(led, LOW);
    delay(1000);
    i++;
  }
}
