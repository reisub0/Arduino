#define RFIDPort Serial1

void RFID_Read() {
  rfidString = "";
  while (!RFIDPort.available())
    ; // wait for a character
  while (RFIDPort.available() > 0) {
    int incomingByte = RFIDPort.read();
    rfidString += incomingByte;
    rfidString += ' ';
  }
}

void RFID_Check() {
  unsigned char Read[5] = {0x04, 0x00, 0x01, 0xDB, 0x4B};
  int i;
  for (i = 0; i <= 4; i++) {
    RFIDPort.write(Read[i]);
    Serial.println(Read[i], HEX);
  }
}

void rfidHandler() {
  RFID_Check();
  RFID_Read();
  if (rfidString == "5 0 1 251 242 61 ")
    return;
  else {
    char *rfidTime = getGSMTimestamp();
    led_group();
    readFromWeighBridge();
    char *bridgeTime = getGSMTimestamp();
    connectServer();
    Data_Send(rfidTime, bridgeTime);
  }
}
