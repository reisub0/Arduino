#include "EEPROM.h"
#include "GSM_Helpers.h"

// ThreadController that will controll all threads
ThreadController controll = ThreadController();

// Thread to handle incoming SMSes
Thread SMSThread = Thread();
Thread statusThread = Thread();
Thread RPCThread = Thread();
EEPROMStruct Settings;

void initThread(Thread *a, void (*ThreadHandler)(void), int interval) {
  a->onRun(ThreadHandler);
  a->setInterval(interval);
  controll.add(a);
}

void statusHandler() {
  tryUntilResponse("AT", "OK");
  updateLocation();
  printLoc();
}

void unreadSMSHandler() {
  if (unreadSMSFlag) {
    Serial.println(F("Unread SMS Detected! Running SMS Code"));
    // Guaranteed that the message will be at position 1
    readMessage();

    // Parse and process SMS (Execute action depending on command)
    processSMS();

    deleteAllMessages();

    unreadSMSFlag = false;
  }
}

void RPCHandler() {
  if (RPC != NULL) {
    processRPC();
    free(RPC);
    RPC = NULL;
  }
}
