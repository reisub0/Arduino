#include <AltSoftSerial.h>
#include <EEPROM.h>

#undef DAYS_PER_WEEK
// #define gpsPort Serial1
// #define GPS_PORT_NAME "Serial1"
// #define DEBUG_PORT Serial

// #include <NMEAGPS.h>

#include <SoftwareSerial.h>

#define baudrate 9600
#define BUZZER 8
#define EEPROMAddr 0
#define GSM Serial3

#define DEBUG

#ifdef DEBUG
#define D(x) x
#else
#define D(x)
#endif

#define RFID1 Serial1
#define RFID2 Serial2
// #define RFID3 Serial3
AltSoftSerial RFID3; // Uses 48,46 as Pins
SoftwareSerial RFID4(50, 51);
// #define RFID4 Serial

// static NMEAGPS gps; // This parses the GPS characters
// static gps_fix fix; // This holds on to the latest values

struct EEPROMStruct {
  bool GSMResetFlag;
  char IMEI[20];
  char APN[20];
  char IP[20];
  char port[10];
  int delaytime;
};

// Since there is no SMS detect thread preconfigure

// EEPROMStruct Settings;
EEPROMStruct Settings = {false,   "0", "airtelgprs.net", "reisub.ddns.net",
                         "40331", 2};

/* void setup() { */
/* Serial.begin(baudrate); */
/* D(Serial.println("# Out setup()")); */
/* Serial.println("Started up"); */
/* } */

void setup() {
  Serial.begin(baudrate);
  D(Serial.println("# In setup()"));

  // EEPROM.put(EEPROMAddr, Settings);
  // EEPROM.get(EEPROMAddr, Settings);

  /* GSM.begin(baudrate); */
  /* gpsPort.begin(baudrate); */
  RFID1.begin(baudrate);
  RFID2.begin(baudrate);
  RFID3.begin(baudrate);
  RFID4.begin(baudrate);

  pinMode(BUZZER, OUTPUT);

  /* updateLocation(); */

  // setTimeFromGSM();
  // EEPROM.put(EEPROMAddr, Settings);
  /* endData(); */
  /* initGSM(); */
  /* initGPRS(); */
  /* updateIMEI(); */
  /* connectServer(); */
  D(Serial.println("# Out setup()"));
  D(Serial.println("Started up"));
}

char RFIDBuff[200] = {0};

// TEST LOOP
/* void loop() { */
/*   // while (RFID1.available()) { */
/*   //   D(Serial.print(RFID1.read())); */
/*   // } */
/*   // D(Serial.println()); */
/*   D(Serial.print("Dryscale : ")); */
/*   D(Serial.println(getWeight(1))); */
/*   D(Serial.print("Wetscale : ")); */
/*   D(Serial.println(getWeight(2))); */
/*   // if (RFID1.available()) { */
/*   /1* D(Serial.print("RFID1: ")); *1/ */
/*   /1* RFID1.readBytes(RFIDBuff, 12); *1/ */
/*   /1* D(Serial.println(RFIDBuff)); *1/ */
/*   /1* connectServer(); *1/ */
/*   /1* RFIDSend(1); *1/ */
/*   /1* buzz(); *1/ */
/*   delay(500); */
/*   // sendData(RFIDBuff); */
/*   delay(5000); */
/* } */

void loop() {
  // while (RFID1.available()) {
  //   D(Serial.print(RFID1.read()));
  // }
  // D(Serial.println());
  if (RFID1.available()) {
    D(Serial.print("RFID1: "));
    RFID1.readBytes(RFIDBuff, 12);
    D(Serial.println(RFIDBuff));
    buzz();
    // delay(5000);
    /* RFIDSend(1); */
    // sendData(RFIDBuff);
  }
  if (RFID2.available()) {
    D(Serial.print("RFID2: "));
    RFID2.readBytes(RFIDBuff, 12);
    D(Serial.println(RFIDBuff));
    buzz();
    // delay(5000);
    /* RFIDSend(2); */
  }
  if (RFID3.available()) {
    D(Serial.print("RFID3: "));
    RFID3.readBytes(RFIDBuff, 12);
    D(Serial.println(RFIDBuff));
    buzz();
    // delay(5000);
    /* RFIDSend(2); */
  }
  RFID4.listen();
  if (RFID4.available()) {
    D(Serial.print("RFID4: "));
    RFID4.readBytes(RFIDBuff, 12);
    D(Serial.println(RFIDBuff));
    buzz();
    // delay(5000);
    /* RFIDSend(2); */
  }
  // while (RFID2.available()) {
  //   D(Serial.print(RFID2.read()));
  // }
  // if (RFID2.available()) {
  //   RFID2.readBytes(RFIDBuff, 12);
  //   D(Serial.println(RFIDBuff));
  // }
}

void buzz() {
  digitalWrite(BUZZER, 1);
  delay(5);
  digitalWrite(BUZZER, 0);
}

void RFIDSend(int rfidNumber) {
  char message[200] = {0};
  connectServer();
  endData();
  startData();
  delay(100);
  GSM.print('$');
  // sendLocation();
  GSM.print(',');
  GSM.print(RFIDBuff);
  /* GSM.print(','); */
  RFIDBuff[12] = ',';
  RFIDBuff[13] = '0' + rfidNumber;
  RFIDBuff[14] = '\0';
  sendWeight(rfidNumber);
  // switch (rfidNumber) {
  // case 1:
  //   // strcat(RFIDBuff, ",1");
  //   RFIDBuff[12] = ',';
  //   RFIDBuff[13] = '1';
  //   RFIDBuff[14] = '\0';
  //   sendWeight(1);
  //   break;
  // case 2:
  //   // strcat(RFIDBuff, ",2");
  //   RFIDBuff[12] = ',';
  //   RFIDBuff[13] = '2';
  //   RFIDBuff[14] = '\0';
  //   sendWeight(2);
  //   break;
  // }
  GSM.print('#');
  endData();
  // strcpy(message, "$");
  // strcat(message, getLocation());
  // strcat(message, ",");
  // strcat(message, RFIDBuff);
  // strcat(message, ",");
  // strcat(message, getWeight(rfidNumber));
  // strcat(message, "#");
  // sendData(message);
  // RFIDBuff[12] = '\0';
}
